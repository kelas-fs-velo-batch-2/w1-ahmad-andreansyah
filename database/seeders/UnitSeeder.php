<?php

namespace Database\Seeders;

use Illuminate\Database\Seeder;
use App\Models\Units;
class UnitSeeder extends Seeder
{
    /**
     * Run the database seeds.
     *
     * @return void
     */
    public function run()
    {
        $perusahaan = Units::create([
            'name'=>'Perusahaan X',
        ]);

        Units::create([
            'name'=>'Department Penjualan',
            'parent_unit_id'=> $perusahaan->id,
        ]);
    }
}
