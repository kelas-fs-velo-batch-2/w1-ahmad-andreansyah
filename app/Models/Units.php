<?php

namespace App\Models;

use Illuminate\Database\Eloquent\Factories\HasFactory;
use Illuminate\Database\Eloquent\Model;

class Units extends Model
{
    use HasFactory;

    public function parent_unit(){
        return $this->belongsTo(Units::class,'parent_unit_id    ');
    }
}
